# Room reservation system
## Prerequisites
* A [MessageBird](https://www.messagebird.com/en/) account and API key is needed for sending messages to users
* A [Twilio](https://www.twilio.com) account and API key is needed for sending messages during the verification process
* A SMTP mail account is needed for sending emails
* A MySQL database is needed for storing the requests

## Deployment
The project is based on Laravel, can basically be deployed as any other Laravel app. Read Laravel [Installation](https://laravel.com/docs/5.6/installation) and [Deployment](https://laravel.com/docs/5.6/deployment) guides for more information.   

1. Clone repository
2. Install dependencies using composer
3. Setup .env file (sample given)
3. Put MessageBird, Twilio, SMTP and MySQL credentials into the ```.env``` file.
4. Run [Migrations](https://laravel.com/docs/5.6/migrations) to generate database structure: ```php artistan migrate:fresh --seed```. There is a [Seeder](https://gitlab.com/fxkopp/room-reservation/blob/development/database/seeds/RoomsTableSeeder.php) for generating the basic room structure.
5. [Create yourself an user](https://stackoverflow.com/a/35754495)
6. 🎉 You're set up!

## Understanding the database structure
* Stockwerkssprecher are kept in ```users``` table.   
    Use ```http://app-domain/register``` endpoint to register new users (takes automatically care about formatting mobile number, endpoint is only available to logged in users).
* Assign users to rooms: ```room_user```. The following roles are available:  
    * manager - 1. Stockwerkssprecher
    * assistant-manager - 2. Stockwerkssprecher
    * foreign-manager - only one per room, is responsible for bookings from tenants not living on the same floor as the room
* ```bookings``` are kept in this table
* ```message_logs``` contains logs about send messages for debugging purpose

There is no admin dashboard yet, but feel free to contribute to the project.