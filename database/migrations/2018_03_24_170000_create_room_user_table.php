<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_user', function (Blueprint $table) {
            // id
            $table->increments('id');
            // room
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            // user
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // role and range
            $table->enum('role', ['foreign-manager', 'manager', 'assistant-manager']);
            $table->string('range_from', 4)->nullable();
            $table->string('range_to', 4)->nullable();
            // index
            $table->index(['room_id', 'user_id'], 'room_user_index');
            $table->index(['room_id', 'role'], 'room_role_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_user');
    }
}
