<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            // id
            $table->increments('id');
            // room properties
            $table->string('name')->unique();
            $table->string('room_number', 4)->unique();            
            $table->enum('house', ['Haus 1', 'Haus 2', 'Haus 3', 'Haus 4', 'Haus 5']);
            $table->enum('floor', ['Erdgeschoss', '1. Stock', '2. Stock', '3. Stock', '4. Stock']);
            $table->decimal('deposit', 15, 2)->default(50.00);
            $table->text('terms_of_use')->nullable();
            // calendar properties
            $table->string('backgroundColor')->nullable();
            $table->string('borderColor')->nullable();
            $table->string('textColor')->nullable();
            // timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
