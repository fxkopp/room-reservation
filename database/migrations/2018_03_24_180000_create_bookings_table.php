<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            // id
            $table->increments('id');
            // room
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms');
            // calendard properties
            $table->boolean('allDay')->default(false);
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->string('url')->nullable();
            $table->enum('rendering', ['background', 'inverse-background'])->nullable();
            // tenant properties
            $table->string('tenant_name');
            $table->string('tenant_email');
            $table->string('tenant_phone')->nullable();
            $table->string('tenant_room_number', 4);
            $table->text('tenant_message')->nullable();
            $table->ipAddress('tenant_ip_address');
            // booking properties
            $table->enum('status', ['notverified', 'unconfirmed', 'confirmed', 'declined'])->default('notverified');
            $table->integer('decided_by')->unsigned()->nullable();
            $table->foreign('decided_by')->references('id')->on('users');
            $table->enum('handover_type', ['in_person', 'postbox'])->nullable();
            $table->dateTime('handover_date')->nullable();
            $table->text('reason')->nullable();
            // timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
