<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create JHH default room set
         */

        // House 1
        DB::table('rooms')->insert([
            'name' => 'Eckraum 1120',
            'room_number' => '1120',
            'house' => 'Haus 1',
            'floor' => '1. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Eckraum 1221',
            'room_number' => '1221',
            'house' => 'Haus 1',
            'floor' => '2. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Eckraum 1421',
            'room_number' => '1421',
            'house' => 'Haus 1',
            'floor' => '4. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        // House 2
        DB::table('rooms')->insert([
            'name' => 'Eckraum 2119',
            'room_number' => '2119',
            'house' => 'Haus 2',
            'floor' => '1. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        // House 3
        DB::table('rooms')->insert([
            'name' => 'Eckraum 3121',
            'room_number' => '3121',
            'house' => 'Haus 3',
            'floor' => '1. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Eckraum 3221',
            'room_number' => '3221',
            'house' => 'Haus 3',
            'floor' => '2. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Eckraum 3321',
            'room_number' => '3321',
            'house' => 'Haus 3',
            'floor' => '3. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        // House 4
        DB::table('rooms')->insert([
            'name' => 'Eckraum 4228',
            'room_number' => '4228',
            'house' => 'Haus 4',
            'floor' => '2. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        // House 5
        DB::table('rooms')->insert([
            'name' => 'Eckraum 5128',
            'room_number' => '5128',
            'house' => 'Haus 5',
            'floor' => '1. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Eckraum 5241',
            'room_number' => '5241',
            'house' => 'Haus 5',
            'floor' => '2. Stock',
            'backgroundColor' => '',
            'borderColor' => '',
            'textColor' => '',
        ]);
    }
}
