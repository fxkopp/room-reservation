<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/vergabebedingungen', 'WelcomeController@termsOfUse')->name('termsOfUse');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('bookings/{booking}/decide/{user}', 'BookingController@decide')->name('bookings.decide');
Route::post('bookings/{booking}/decide/{user}', 'BookingController@storeDecision')->name('bookings.storeDecision');

Route::resource('bookings', 'BookingController');

Route::get('verify/{booking}', 'VerifyBookingController@index')->name('verify.index');
Route::post('verify/{booking}', 'VerifyBookingController@checkCode')->name('verify.checkCode');
Route::get('verify/{booking}/resend', 'VerifyBookingController@resendCode')->name('verify.resendCode');

Route::get('/mailable', function () {
    $user = App\User::find(1);    
    $booking = App\Booking::find(1);

    return new App\Mail\BookingConfirmed($booking);
});
