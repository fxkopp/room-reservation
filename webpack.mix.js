let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/instant.scss', 'public/css');

// Fullcalendar
mix.copy('node_modules/fullcalendar/dist/fullcalendar.css', 'public/css/fullcalendar.css')
   .copy('node_modules/fullcalendar/dist/fullcalendar.js', 'public/js/fullcalendar.js')
   .copy('node_modules/fullcalendar/dist/locale/de.js', 'public/js/fullcalendar-de.js')
   .copy('node_modules/moment/moment.js', 'public/js/moment.js');
   


mix.autoload({
    jquery: ['$', 'window.jQuery']
});