<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Room;

class StoreBooking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room' => 'required|exists:rooms,id',
            'date' => 'required|date_format:Y-m-d',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|phone:AUTO,DE',
            'room_number' => 'required|numeric|min:1001|max:5499',
            'message' => 'string|nullable',
            'terms_of_use' => 'accepted',
        ];
    }
}
