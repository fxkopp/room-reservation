<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Booking extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTitle(),
            'allDay' => (boolean) $this->allDay,
            'start' => $this->start->format(\DateTime::ISO8601),
            'end' => $this->end->format(\DateTime::ISO8601),
            'url' => $this->url ?? "",
            'rendering' => $this->rendering ?? "",
            'backgroundColor' => $this->room->backgroundColor,
            'borderColor' => $this->room->borderColor,
            'textColor' => $this->room->textColor,
        ];
    }
}
