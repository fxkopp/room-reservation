<?php

namespace App\Http\Controllers;

use App\Booking;
use App\User;
use App\Room;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Http\Requests\StoreBooking;
use App\Http\Requests\StoreDecision;

use App\Http\Resources\BookingCollection;

use App\Events\UnverifiedBookingCreated;
use App\Events\BookingRequested;
use App\Events\BookingConfirmed;
use App\Events\BookingDeclined;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        return new BookingCollection(
            Booking::where([
                ['status', 'confirmed'],
                ['start', '>=', $request->input('start', Carbon::now())],
                ['end', '<=', $request->input('end', Carbon::now())]
            ])->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('booking.create', [
            'rooms' => Room::orderBy('house', 'asc')->orderBy('name', 'asc')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBooking  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBooking $request)
    {
        // Create new Booking
        $booking = new Booking;

        // Associate the Room
        $booking->room()->associate($request->input('room'));

        // Format start and end
        $booking->start = Carbon::createFromFormat(
            'Y-m-d H:i', 
            $request->input('date') . ' ' . $request->input('start_time'), 
            'Europe/Berlin'
        );

        $booking->end = Carbon::createFromFormat(
            'Y-m-d H:i', 
            $request->input('date') . ' ' . $request->input('end_time'), 
            'Europe/Berlin'
        );

        // Add a day to end if start time is greater than or equal with end time
        if ($booking->start->gte($booking->end)) {
            $booking->end = $booking->end->addDay();
        }

        // Check if the room is occupied at the desired booking time
        if ($booking->room->occupied($booking->start, $booking->end)) {
            // Redirect with flash message
            return back()
                    ->withInput()
                    ->with('occupied', 'Für den gewünschten Zeitraum ist bereits eine Buchung eingetragen!');
        }

        // Set name
        $booking->tenant_name = $request->input('name');

        // Set email
        $booking->tenant_email = $request->input('email');

        // Set ip address
        $booking->tenant_ip_address = $request->ip();

        // Format and set phone number
        // @TODO: Write a setter for this
        $booking->tenant_phone = phone(
            $request->input('phone'),
            ['AUTO', 'DE'],
            \libphonenumber\PhoneNumberFormat::INTERNATIONAL
        );

        // Set room number
        $booking->tenant_room_number = $request->input('room_number');

        // Set message if filled
        if ($request->filled('message')) $booking->tenant_message = $request->input('message');

        // Save to DB
        $booking->save();

        // Check if a verification is required
        // Verification is required if there are no verified bookings with this email address
        if (Booking::where([
            ['tenant_phone', '=', $booking->tenant_phone],
            ['status', '!=', 'notverified']
        ])->exists()) {
            $booking->status = 'unconfirmed';
            $booking->save();

            // Trigger Event
            event(new BookingRequested($booking));

            // Redirect
            return redirect()->route('bookings.show', [
                'booking' => $booking,
            ]);
        } else {
            // Trigger Event
            // @TODO: Make Event Async
            event(new UnverifiedBookingCreated($booking));

            // Redirect
            return redirect()->route('verify.index', [
                'booking' => $booking,
                'hash' => Hash::make($booking->tenant_email . "-JHH-" . $booking->tenant_phone)
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        switch ($booking->status) {
            case 'unconfirmed':
                return view('booking.unconfirmed');
            case 'confirmed':
                return view('booking.confirmed');
            case 'declined':
                return view('booking.declined');
        }
    }

    /**
     * Show the form for deciding on the specified resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
    */
    public function decide(Request $request, Booking $booking, User $user) {
        // Verify hash && authorize request
        if(
            !Hash::check($booking->getAuthorizationHashVariables($user), $request->input('hash', null))
            OR !$user->can('decide', $booking)
        ) {
            abort(403, 'Unauthorized action.');
        }

        return view('booking.decide', [
            'booking' => $booking,
            'user' => $user,
        ]);
    }

    /**
     * Store the decision for the specified resource
     *
     * @param  \App\Http\Requests\StoreDecision  $request
     * @param  \App\Booking  $booking
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function storeDecision(StoreDecision $request, Booking $booking, User $user)
    {
        // Verify hash && authorize request
        if(
            !Hash::check($booking->getAuthorizationHashVariables($user), $request->input('hash', null))
            OR !$user->can('decide', $booking)
        ) {
            abort(403, 'Unauthorized action.');
        }

        // Associate user who made the decision with the booking
        $booking->decidedBy()->associate($user);

        // Change database entry depending on decision
        switch ($request->input('type')) {
            case 'confirm':
                // Check if the room is occupied at the desired booking time
                if ($booking->room->occupied($booking->start, $booking->end)) {
                    // Redirect with flash message
                    return back()
                            ->withInput()
                            ->with('occupied', 'Für den gewünschten Zeitraum ist bereits eine Buchung eingetragen! Du kannst diese Anfrage nur ablehnen.');
                }

                // Update variables
                $booking->status = 'confirmed';
                $booking->handover_type = $request->input('handover');
                $booking->handover_date = Carbon::createFromFormat(
                    'Y-m-d H:i', 
                    $request->input('date') . ' ' . $request->input('time'), 
                    'Europe/Berlin'
                );
                $booking->save();

                // Fire event
                event(new BookingConfirmed($booking));
                break;
            case 'decline':
                // Update variables
                $booking->status = 'declined';
                $booking->reason = $request->input('reason');
                $booking->save();

                // Fire event
                event(new BookingDeclined($booking));                
                break;
        }

        // Redirect
        return redirect()->route('bookings.show', [
            'booking' => $booking,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
