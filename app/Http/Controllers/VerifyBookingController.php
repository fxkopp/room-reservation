<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Events\BookingRequested;
use App\Events\UnverifiedBookingCreated;

use Authy\AuthyApi as AuthyApi;

class VerifyBookingController extends Controller
{
    /**
     * Display the verify page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Booking $booking)
    {
        // Check hash code
        if (!Hash::check(
            $booking->tenant_email . "-JHH-" . $booking->tenant_phone, 
            $request->input('hash', null))
        ) {
            abort(403, 'Unauthorized action.');
        }

        return view('verify.index', [
            'booking' => $booking,
        ]);
    }

    /**
     * Check the verification code.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @param  \Authy\AuthyApi $authyApi
     * @return \Illuminate\Http\Response
     */
    public function checkCode(Request $request, Booking  $booking, AuthyApi $authyApi)
    {
        $request->validate([
            'verification_code' => 'required|digits:4',
        ]);

        // Check verification code
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $authyResponse = $authyApi->phoneVerificationCheck(
            phone($booking->tenant_phone, ['AUTO', 'DE'], \libphonenumber\PhoneNumberFormat::NATIONAL),
            $phoneNumberUtil->parse($booking->tenant_phone, ['AUTO', 'DE'])->getCountryCode(),
            $request->input('verification_code')
        );

        if ($authyResponse->ok()) {
            // Verified
            $booking->status = 'unconfirmed';
            $booking->save();
            // Trigger Event
            event(new BookingRequested($booking));
            // Redirect
            return redirect()->route('bookings.show', [
                'booking' => $booking,
            ]);
        } else {
            // Redirect back with flash message
            return back()->with('twilioError', 'Beim SMS Versand ist ein Fehler aufgetreten. Bitte versuche es noch einmal.');
        }
    }

    /**
     * Resend the verification code.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function resendCode(Booking $booking)
    {
        // Check if the last verification request is at least 2min back
        $verifiedInLastTwoMinutes = \App\MessageLog::where([
            ['booking_id', $booking->id],
            ['type', 'verification'],
            ['created_at', '>=', \Carbon\Carbon::now()->subMinutes(2)->format('Y-m-d H:i')]
        ])->exists();

        // Redirect back with error message if user has to wait
        if ($verifiedInLastTwoMinutes) {
            return back()->with('twilioError', 'Ein Verifikationsversuch ist nur alle 2 Minuten möglich.');
        }

        // Trigger Verification event
        event(new UnverifiedBookingCreated($booking));

        // Redirect back with flash message
        return back()->with('twilioSuccess', 'Der Verifikationscode wurde erneut versandt.');
    }
}
