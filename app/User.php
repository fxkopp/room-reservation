<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Room;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'room_number', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The rooms that are managed by the user.
     */
    public function rooms()
    {
        return $this->belongsToMany('App\Room')->withPivot('role');
    }

    /**
     * Check if user can manage specific room.
     * 
     * @param  \App\Room  $room
     * @retrun boolean
     */
    public function canManage(Room $room) {
        return $this->rooms->contains($room);
    }
}
