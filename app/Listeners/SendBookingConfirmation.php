<?php

namespace App\Listeners;

use App\Events\BookingConfirmed;
use App\Mail\BookingConfirmed as BookingConfirmedMail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use MessageBird\Client as MessageBirdClient;

class SendBookingConfirmation
{
    /**
     * MessageBird Client
     */
    private $messageBird;

    /**
     * Create the event listener.
     *
     * @param  \MessageBird\Client $messageBird
     * @return void
     */
    public function __construct(MessageBirdClient $messageBird)
    {
        $this->messageBird = $messageBird;
    }

    /**
     * Handle the event.
     *
     * @param  BookingConfirmed  $event
     * @return void
     */
    public function handle(BookingConfirmed $event)
    {
        // Send SMS to Booker
        $booking = $event->booking;
        $message = new \MessageBird\Objects\Message();
        $message->originator = "Hiltnerheim";
        $message->recipients = [
            str_replace('+', '', str_replace(' ', '', $booking->tenant_phone))
        ];
        $message->body =
            "Buchungsanfrage für " . $booking->room->name . 
            " am " . $booking->start->format('d.m.Y') . 
            " von " . $booking->start->format('H:i') . 
            " bis " . $booking->end->format('H:i') . " Uhr" . 
            " wurde genehmigt. Details findest du in der E-Mail.";

        try {
            // Send SMS
            $messageResult = $this->messageBird->messages->create($message);
            foreach ($messageResult->recipients->items as $r) {
                $this->log($booking, $r->status . ' to ' . $r->recipient);
            }
            $this->log($booking, $messageResult);
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            $this->log($booking, 'Invalid access key.');
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            $this->log($booking, 'Out of balance.');
        } catch (\Exception $e) {
            $this->log($booking, $e->getMessage());
        }

        /** 
         * Send a mail to the Booker
         * BCC to every Manager, but exclude the Manager who decided on the booking
         */
        $managers = $event->booking->getManagers();
        Mail::to($event->booking->tenant_email)
            ->bcc(
                $managers['managers']
                    ->merge($managers['assistant-managers'])
                    ->map(function($item, $key) {
                        return $item->email;
                    })
                    ->diff($event->booking->decidedBy->email)
                    ->unique()
                    ->all()
            )
            ->send(new BookingConfirmedMail($event->booking));
    }

    private function log($booking, $response) {
        return \App\MessageLog::create([
            'booking_id' => $booking->id,
            'type' => 'confirmation', 
            'provider' => 'messagebird',
            'response' => $response,
        ]);
    }
}
