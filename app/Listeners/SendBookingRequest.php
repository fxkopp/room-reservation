<?php

namespace App\Listeners;

use App\Events\BookingRequested;
use App\Mail\BookingRequested as BookingRequestedMail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use MessageBird\Client as MessageBirdClient;

class SendBookingRequest
{
    /**
     * MessageBird Client
     */
    private $messageBird;

    /**
     * Create the event listener.
     *
     * @param  \MessageBird\Client $messageBird
     * @return void
     */
    public function __construct(MessageBirdClient $messageBird)
    {
        $this->messageBird = $messageBird;
    }

    /**
     * Handle the event.
     *
     * @param  BookingRequested  $event
     * @return void
     */
    public function handle(BookingRequested $event)
    {
        $managers = $event->booking->getManagers();
        $booking = $event->booking;

        $message = new \MessageBird\Objects\Message();
        $message->originator = "Hiltnerheim";
        $message->recipients = $managers['managers']
                                    ->map(function($item, $key) {
                                        return str_replace('+', '', str_replace(' ', '', $item->phone));
                                    })
                                    ->unique();
        $message->body =
            "Buchungsanfrage von " . $booking->tenant_name . 
            " (Zimmer " . $booking->tenant_room_number . ")" .
            " für " . $booking->start->format('l, d.m.Y') . 
            " von " . $booking->start->format('H:i') . 
            " bis " . $booking->end->format('H:i') . " Uhr." . 
            " Details findest du in der E-Mail.";

        try {
            // Send SMS
            $messageResult = $this->messageBird->messages->create($message);
            foreach ($messageResult->recipients->items as $r) {
                $this->log($booking, $r->status . ' to ' . $r->recipient);
            }
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            $this->log($booking, 'Invalid access key.');
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            $this->log($booking, 'Out of balance.');
        } catch (\Exception $e) {
            $this->log($booking, $e->getMessage());
        }
        
        // Send a Mail to the main managers
        foreach ($managers['managers'] as $user) {
            Mail::to($user->email)->send(new BookingRequestedMail($user, $booking));
        }
        // Send a Mail to the assistant managers
        foreach ($managers['assistant-managers'] as $user) {
            Mail::to($user->email)->send(new BookingRequestedMail($user, $booking));
        }
    }

    private function log($booking, $response) {
        return \App\MessageLog::create([
            'booking_id' => $booking->id,
            'type' => 'request', 
            'provider' => 'messagebird',
            'response' => $response,
        ]);
    }
}
