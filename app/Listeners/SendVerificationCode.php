<?php

namespace App\Listeners;

use App\Events\UnverifiedBookingCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Authy\AuthyApi as AuthyApi;

class SendVerificationCode
{
    /**
     * Create the event listener.
     *
     * @param  \Authy\AuthyApi $authyApi
     * @return void
     */
    public function __construct(AuthyApi $authyApi)
    {
        $this->authyApi = $authyApi;
    }

    /**
     * Handle the event.
     *
     * @param  UnverifiedBookingCreated  $event
     * @return void
     */
    public function handle(UnverifiedBookingCreated $event)
    {
        $booking = $event->booking;
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        $authyResponse = $this->authyApi->phoneVerificationStart(
            phone($booking->tenant_phone, ['AUTO', 'DE'], \libphonenumber\PhoneNumberFormat::NATIONAL),
            $phoneNumberUtil->parse($booking->tenant_phone, ['AUTO', 'DE'])->getCountryCode(),
            'sms'
        );

        // Log Twilio response
        $this->log($booking, $authyResponse->message());
    }

    private function log($booking, $response) {
        return \App\MessageLog::create([
            'booking_id' => $booking->id,
            'type' => 'verification', 
            'provider' => 'twilio',
            'response' => $response,
        ]);
    }
}
