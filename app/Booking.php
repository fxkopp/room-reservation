<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Booking extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start', 'end', 'handover_date'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['title'];

    /**
     * Get the room the booking belongs to.
     */
    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    /**
     * Get the room the booking belongs to.
     */
    public function decidedBy()
    {
        return $this->belongsTo('App\User', 'decided_by');
    }

    /**
     * Get the title.
     *
     * @return bool
     */
    public function getTitle() {
        // return $this->room->name . ": " . $this->start->format('H:i') . " - " . $this->end->format('H:i');
        return $this->room->name;        
    }

    /**
     * Return users that are able to decide on the booking based on the room and and the bookers room number.
     * 
     * @return array
     */
    public function getManagers() {
        // Foreign bookings are handled differently
        if ($this->room->house !== $this->getBookerHouse()) {
            $foreignManagers = $this->room->managers('foreign-manager');
            return [
                // Only the foreign managers are allowed to decide on bookings
                'managers' => $foreignManagers,
                // All normal managers are treated as assistant managers
                'assistant-managers' => $this->room->managers()->diff($foreignManagers)->all(),
            ];
        }

        $booking = $this;
        return [
            'managers' => $this->room->managers('manager')
                            ->filter(function ($value, $key) use($booking) {
                                return (
                                    (($value->pivot->range_from <= $booking->tenant_room_number) || is_null($value->pivot->range_from))
                                    &&
                                    (($value->pivot->range_to >= $booking->tenant_room_number) || is_null($value->pivot->range_to))
                                );
                            }),
            'assistant-managers' => $this->room->managers('assistant-manager')
                            ->filter(function ($value, $key) use($booking) {
                                return (
                                    (($value->pivot->range_from <= $booking->tenant_room_number) || is_null($value->pivot->range_from))
                                    &&
                                    (($value->pivot->range_to >= $booking->tenant_room_number) || is_null($value->pivot->range_to))
                                );
                            }),
        ];
    }

    /**
     * Returns the house of the booker based on the bookers room number.
     * 
     * @return String
     */
    private function getBookerHouse() {
        switch(substr($this->tenant_room_number, 0, 1)) {
            case '1':
                return 'Haus 1';
            case '2':
                return 'Haus 2';
            case '3':
                return 'Haus 3';
            case '4':
                return 'Haus 4';
            case '5':
                return 'Haus 5';
            default: 
                return null;
        } 
    }
    
    /**
     * Return authorization hash variables for booking decision.
     * 
     * @param  \App\User  $user
     * @return String
     */
    public function getAuthorizationHashVariables(User $user) {
        return implode("_", [$this->id, $user->id, $user->api_key]);
    }

    /**
     * Return authorization hash for booking decision.
     * 
     * @param  \App\User  $user
     * @return String
     */
    public function getAuthorizationHash(User $user) {
        return Hash::make($this->getAuthorizationHashVariables($user));
    }
}
