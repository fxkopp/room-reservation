<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\UnverifiedBookingCreated' => [
            'App\Listeners\SendVerificationCode',
        ],
        'App\Events\BookingRequested' => [
            'App\Listeners\SendBookingRequest',
        ],
        'App\Events\BookingConfirmed' => [
            'App\Listeners\SendBookingConfirmation',
        ],
        'App\Events\BookingDeclined' => [
            'App\Listeners\SendBookingDeclination',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
