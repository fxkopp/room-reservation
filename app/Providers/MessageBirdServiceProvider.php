<?php

namespace App\Providers;

use MessageBird\Client as MessageBirdClient;
use Illuminate\Support\ServiceProvider;

class MessageBirdServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MessageBirdClient::class, function ($app) {
            $accessKey = getenv('MESSAGE_BIRD_ACCESS_KEY') or die(
                "You must specify your access key for MessageBird."
            );

            return new MessageBirdClient($accessKey);
        });
    }
}
