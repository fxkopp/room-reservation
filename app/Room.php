<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The users managing the room.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('role', 'range_from', 'range_to');
    }

    /**
     * Get users managing the room with a specific role.
     * 
     * @param  String  $role
     */
    public function managers($role = 'manager') {
        return $this->users()->where('role', $role)->get();
    }

    /**
     * Check if there is a confirmed booking between start and end time.
     */
    public function occupied($start, $end) {
        return DB::table('bookings')
                ->where('room_id', '=', $this->id)
                ->where('status', '=', 'confirmed')
                ->where(function ($query) use ($start, $end) {
                    $query->where(
                        function ($query) use ($start, $end) {
                            $query->where('start', '<=', $start)
                                  ->where('end', '>=', $start);
                        }
                    )->orWhere(
                        function ($query) use ($start, $end) {
                            $query->where('start', '<=', $end)
                                  ->where('start', '>=', $end);
                        }
                    )->orWhere(
                        function ($query) use ($start, $end) {
                            $query->where('start', '>=', $start)
                                  ->where('end', '<=', $end);
                        }
                    );
                })->exists();
    }
}
