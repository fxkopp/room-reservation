@extends('layouts.app')

@section('content')
<div class="container-fluid">

     {{-- <div class="jumbotron">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
        </p>
    </div>  --}}

    <div class="row bg-dark text-white">
        <div class="col-md-9 p-3 p-md-5 my-md-3 mx-md-auto">
            <h1 class="display-4">Übersicht / Kalender</h1>
        </div>
        <div class="col-md-2 p-0 m-auto offset-md-1">
            <a class="btn btn-outline-primary btn-lg btn-block" href="{{ route('bookings.create') }}" role="button">
                Buchungsanfrage 
            </a>
            <a class="btn btn-outline-secondary btn-lg btn-block" href="{{ route('termsOfUse') }}" role="button">
                Vergabebedingungen 
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9 p-3 p-md-5 mx-md-auto">
            <div id='calendar'></div>
        </div>
        <div class="col-md-2 py-3 py-md-5 mx-md-auto offset-md-1">
            <h2>HowTo</h2>
            <p>Im Kalender einen freien Zeitraum suchen und Eckraum online buchen.</p>
            <p>Schlüssel abholen und Kaution hinterlegen.</p>
            <p>Raum ordentlich verlassen und Schlüssel wie vereinbart zurückgeben.</p>
            <p>Es gelten die Vergabebedingungen.</p>
        </div>
    </div>
</div>
@endsection

@section('custom-scripts')
$(function() {

  // page is now ready, initialize the calendar...

  $('#calendar').fullCalendar({
      locale: 'de',
      defaultView: 'agendaWeek',
      themeSystem: 'bootstrap4',
      allDaySlot: false,
      slotEventOverlap: false,
      nowIndicator: true,
      minTime: "08:00:00",
      scrollTime: "12:00:00",
      events: '/bookings'
  });

});
@endsection
