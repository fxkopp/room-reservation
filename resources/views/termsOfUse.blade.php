@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row bg-dark text-white">
        <div class="col-md-9 p-3 p-md-5 my-md-3 mx-md-auto">
            <h1 class="display-4">Vergabebedingungen</h1>
        </div>
        <div class="col-md-2 p-0 m-auto offset-md-1">
            <a class="btn btn-outline-primary btn-lg btn-block" href="{{ route('welcome') }}" role="button">
                Kalender / Übersicht 
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 p-3 p-md-5 mx-md-auto">
            <h2>Allgemeines</h2>
            <p>
                Die Küchen und Gemeinschaftsräume im JHH können an Wochenenden und vor Feiertagen (ggf. auch unter Woche) an Bewohner des
                Wohnheimes, für private Feiern (Examensfeiern, Stockwerksfeiern, Hochzeiten, Geburtstage etc.) überlassen werden, falls davon
                auszugehen ist, dass das Gemeinschaftsleben dadurch gefördert wird, andere Heimbewohner nicht belästigt werden und fremdes Eigentum
                nicht beeinträchtigt wird. Eine Vergabe an Vereine oder sonstige Gruppen, die zum Leben innerhalb des JHH keinen Bezug haben, ist nicht
                zulässig.
            </p>
            <h2>Bestimmungen</h2>
            <ol>
                <li>Voraussetzung für die Überlassung der Küchen/der Gemeinschaftsräume ist die Zustimmung der zu hörenden Personen . Dazu ist jeweils eine Genehmigung nötig (Buchungssystem).</li>
                <li>Die Teilnehmerzahl ist auf maximal 20 Personen beschränkt.</li>
                <li>Um Gäste und Besucher den Zugang zum Heim zu ermöglichen, ist es untersagt Haustüren unbeaufsichtigt offenstehen zu lassen oder mit anderen Hilfsmitteln am Schließen zu hindern. Einlass ins Heim kann mit geeigneten Mitteln (z.B. durch Anruf auf Mobil-Telefon) gewährt werden.</li>
                <li>Der Veranstalter haftet für Schäden, die durch ihn, seine Gäste und Besucher am Heim und dessen Inventar verursacht werden.</li>
                <li>Das im ganzen Haus geltende Rauchverbot ist einzuhalten.</li>
                <li>Eine Kaution ist bei der zuständigen Person abzugeben.</li>
                <li>
                    Der Hausfrieden darf durch die Veranstaltung nicht gestört werden. Es muss darauf geachtet werden, dass die Bewohner durch die Veranstaltung nicht unzumutbar gestört werden.
                    <ol>
                        <li>Spätestens ab 23 Uhr ist Zimmerlautstärke einzuhalten. Auch vor 23 Uhr ist in Rücksicht auf die anderen Heimbewohner übermäßiger Lärm z.B. durch zu laute Musik, zu vermeiden.</li>
                        <li>Die Veranstalter müssen Ihre Gäste und Besucher darauf hinweisen, dass sie sich beim Verlassen des Hauses so zu verhalten haben, dass weder Anlieger noch die Heimbewohner gestört werden.</li>
                        <li>Muss die Heimleitung, ein Senior od. der Hausmeister berechtigterweise wegen Ruhestörung nach 00 Uhr zur Veranstaltung kommen, werden ein Betrag von 15 Euro eingefordert, im Wiederholungsfalle 30 Euro. Wird den Anweisungen der vorher genannten Personen nicht Folge geleistet, wird das Hausrecht notfalls mit Hilfe der Polizei durchgesetzt.</li>
                    </ol>
                </li>
                <li>Die Benutzung von in den Küchen und Gemeinschaftsräumen befindlichen Geräten, Einrichtungen und Lebensmitteln ist nur nach vorheriger Genehmigung durch Senioren, oder Heimleitung erlaubt. Für Schäden, die durch deren Gebrauch entstehen, haften die Veranstalter.</li>
                <li>Der Veranstaltungsraum ist unmittelbar nach dem Fest sorgfältig zu reinigen. Spätestens bis zum Mittag des nächsten Tages müssen diese Räumlichkeiten gereinigt sein. Ist der Raum nicht oder ungenügend gereinigt, kann die Verwaltung - auf Kosten des Veranstalters - ein Putzunternehmen beauftragen die Reinigung durchzuführen.</li>
                <li>Fallen durch ein Fest größere Mengen Restmüll an (Mülltrennung wird vorausgesetzt) und wird dieser über die Hausmüllanlage entsorgt, werden die Entsorgungsgebühren auf den Veranstalter umgelegt.</li>
                <li>Bei Verstößen gegen diesen Vertrag und/oder die <a href="http://hiltnerheim.de/content_hausordnung.php" target="_blank">Hausordnung</a> haben der Heimleiter und der Senior/die Seniorin das Recht, die Veranstaltung sofort zu beenden.</li>
                <li>Die Bestimmungen des Mietvertrages mit der Protestantischen Alumneumsstiftung undder Hausordnung bleiben unberührt.</li>
            </ol>
            <h2>Sonderbedingungen Wohnheimsküche (Haus 4)</h2>
            <ul>
                <li>Nicht für Partys, sondern zum Kochen / Backen.</li>
                <li>Nutzungsgebühr 1 € (zum Nachkaufen von Ölen, Gewürzen,Spülmittel, etc.)</li>
                <li>Nur selbstmitgebrachte Lebensmittel und Getränke verwenden (Vorrat gehört dem Kochtutorium) - Öle und Gewürze dürfen verwendet werden.</li>
                <li>Falls etwas aufgebraucht wird (z.B. Pfeffer) bitte den Tutoren Bescheid sagen!</li>
                <li>Schlüsselrückgabe: spät. 8 Uhr des Folgetags oder nach Absprache</li>
                <li>Die Küche muss sauber und ordentlich hinterlassen werden, ansonsten wird ein Teil der Kaution einbehalten. (Oberflächen sauber wischen, abspülen, alles wieder einräumen und Boden sauber machen, wenn etwas ausgeschüttet o.ä.)</li>
                <li>Bitte geht vorsichtig und sorgsam mit dem Sachen um!</li>
            </ul>
        </div>
    </div>
</div>
@endsection
