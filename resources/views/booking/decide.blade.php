@extends('layouts.instant')

@section('content')
<div class="container">
    <div class="py-5 text-center">
        <div class="d-block mx-auto mb-4" style="font-size:7em; color:#ffc107"> <!-- #6c757d -->
            <i class="fas fa-balance-scale"></i>
        </div>
        <h2>
            Buchungsanfrage von {{ $booking->tenant_name }}
        </h2>
        <h4>
            {{ $booking->room->name }}
        </h4>
        <h5>
            {{ $booking->start->format('l, d.m.Y')}} 
            von {{ $booking->start->format('H:i') }} bis {{ $booking->end->format('H:i') }} Uhr
        </h5>

        <div class="mt-5 row">
            <div class="col-10 mx-auto">
                <!-- Error Messages -->
                @include('shared.errors')
                @if ($message = Session::get('occupied'))
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-12 col-sm-6 text-left decide-border mb-2 mb-md-0">
                        <!-- Form -->
                        <form method="POST" action="{{ route('bookings.storeDecision', [
                            'booking' => $booking,
                            'user' => $user,
                            'hash' => $booking->getAuthorizationHash($user),
                            'type' => 'confirm',
                        ]) }}">

                            <!-- CSRF token -->
                            {{ csrf_field() }}
                            <div style="min-height: 137px;">
                                <!-- Date and Time -->
                                <div class="form-group">
                                    <label for="inputDate"><b>Termin zur Schlüsselübergabe:</b></label>
                                    <div class="row">
                                        <div class="col-8">
                                            <input type="date" class="form-control" id="inputDate" name="date"
                                            value="{{ old('date', null) }}" required>
                                        </div>
                                        <div class="col-4">
                                            <input type="time" class="form-control" id="inputTime" name="time"
                                            value="{{ old('time', null) }}" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Type of Handover -->
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="handover" id="radioInPerson" value="in_person"
                                        @if(old('handover', 'in_person') == 'in_person') checked @endif>
                                        <label class="form-check-label" for="radioInPerson">
                                            Persönliche Übergabe (vor {{ $booking->room->name }})
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="handover" id="radioPostbox" value="postbox"
                                        @if(old('handover') == 'postbox') checked @endif>                                        
                                        <label class="form-check-label" for="radioPostbox">
                                            Schlüsseleinwurf in den Briefkasten ({{ $booking->tenant_room_number }})
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg btn-block">Anfrage annehmen</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-sm-6 text-left">
                        <!-- Form -->
                        <form method="POST" action="{{ route('bookings.storeDecision', [
                            'booking' => $booking,
                            'user' => $user,
                            'hash' => $booking->getAuthorizationHash($user),
                            'type' => 'decline',
                        ]) }}">

                            <!-- CSRF token -->
                            {{ csrf_field() }}

                            <!-- Reason -->
                            <div class="form-group">
                                <label for="textareaReason"><b>Grund:</b></label>
                                <textarea class="form-control" name="reason" id="textareaReason" rows="4" required>{{ old('reason') }}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-danger btn-lg btn-block">Anfrage ablehnen</button>
                            </div>
                        </form>
                    </div>                    
                </div>              
            </div>
        </div>

    </div>
</div>
@endsection