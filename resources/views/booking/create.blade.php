@extends('layouts.app')

@section('content')
<div class="row mb-md-5 bg-dark text-white">
    <div class="col-md-9 p-3 p-md-5 my-md-3 mx-md-auto">
        <h1 class="display-4">Buchungsanfrage</h1>
    </div>
    <div class="col-md-2 p-0 m-auto offset-md-1"></div>
</div>

<div class="container mb-md-5">

    <!-- Error Messages -->
    @include('shared.errors')

    @if ($message = Session::get('occupied'))
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif
    
    <!-- Form -->
    <form method="POST" action="{{ route('bookings.store') }}">

        <!-- CSRF token -->
        {{ csrf_field() }}   

        <!-- Type -->
        <div class="form-group row">
            <label for="selectRoom" class="col-sm-2 col-form-label">
                Raum
            </label>
            <div class="col-sm-10">
                <select class="form-control" id="selectRoom" name="room" aria-describedby="selectRoomHelpBlock">
                    @foreach ($rooms as $room)
                        <option 
                            value="{{ $room->id }}"
                            @if(isset($b) AND $b->type() !== null AND $b->type->id == $room->id) selected @endif>
                                {{ $room->house }} - {{ $room->name }} ({{ $room->floor }})
                        </option>
                    @endforeach
                </select>
                <small id="selectRoomHelpBlock" class="form-text text-muted">
                    Im Normalfall solltest du einen Eckraum aus deinem Haus anmieten.
                </small>
            </div>
        </div>

        <!-- Date and Time -->
        <div class="form-group row">
            <label for="inputDate" class="col-sm-2 col-form-label">
                Datum
            </label>
            <div class="col-sm-4">
                <input 
                    type="date" class="form-control" id="inputDate" name="date"
                    value="{{ old('date', isset($b->date) ? $b->date : null) }}" required>
            </div>
            <label for="inputStartTime" class="col-sm-1 col-form-label">
                von
            </label>
            <div class="col-sm-2">
                <input 
                type="time" class="form-control" id="inputStartTime" name="start_time"
                value="{{ old('start_time', isset($b->start_time) ? $b->start_time : null) }}" required>
            </div>
            <label for="inputEndTime" class="col-sm-1 col-form-label">
                bis
            </label>
            <div class="col-sm-2">
                <input 
                type="time" class="form-control" id="inputEndTime" name="end_time"
                value="{{ old('end_time', isset($b->end_time) ? $b->end_time : null) }}" required>
            </div>
        </div>

        <!-- Name -->
        <div class="form-group row">
            <label for="inputName" class="col-sm-2 col-form-label">
                Name
            </label>
            <div class="col-sm-10">
                <input 
                    type="text" class="form-control" id="inputName" name="name"
                    value="{{ old('name', isset($b->name) ? $b->name : null) }}"
                    placeholder="Max Mustermann" required>
            </div>
        </div>

        <!-- E-Mail -->
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">
                E-Mail
            </label>
            <div class="col-sm-10">
                <input 
                    type="email" class="form-control" id="inputEmail" name="email"
                    value="{{ old('email', isset($b->email) ? $b->email : null) }}"
                    placeholder="mail@example.org" required>
            </div>
        </div>

        <!-- Phone -->
        <div class="form-group row">
            <label for="inputPhone" class="col-sm-2 col-form-label">
                Handynummer
            </label>
            <div class="col-sm-10">
                <input 
                    type="tel" class="form-control" id="inputPhone" name="phone"
                    value="{{ old('phone', isset($b->phone) ? $b->phone : null) }}"
                    placeholder="+49 176 123123123" required>
            </div>
        </div>

        <!-- Room Number -->
        <div class="form-group row">
            <label for="inputRoomNumber" class="col-sm-2 col-form-label">
                Zimmer-Nr.
            </label>
            <div class="col-sm-10">
                <input 
                    type="number" class="form-control" id="inputRoomNumber" name="room_number"
                    value="{{ old('room_number', isset($b->room_number) ? $b->room_number : null) }}"
                    placeholder="2002" min="1001" max="5499" required>
            </div>
        </div>

        <!-- Message -->
        <div class="form-group row">
            <label for="textareaMessage" class="col-sm-2 col-form-label">
                Nachricht
            </label>
            <div class="col-sm-10">
                <textarea 
                    class="form-control" id="textareaMessage" name="message" 
                    placeholder="(optional) Deine Nachricht ..." 
                    rows="3">{{ old('message', isset($b->message) ? $b->message : '') }}</textarea>
            </div>
        </div>

        <!-- Terms Of Use -->
        <div class="form-group row">
            {{--  <div class="col-sm-2">Mietbedingungen</div>  --}}
            <div class="offset-sm-2 col-sm-10">
                <div class="form-check">
                    <input 
                        type="checkbox" class="form-check-input" id="inputCheckboxTermsOfUse"
                        name="terms_of_use" required>
                    <label class="form-check-label" for="inputCheckboxTermsOfUse">
                        Die <a href="{{ route('termsOfUse') }}" target="_blank">Vergabebedingungen</a> habe ich gelesen und akzeptiert
                    </label>
                </div>
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Anfrage absenden</button>
            </div>
        </div>

    </form>
</div>
@endsection


{{--  <!-- Title -->
<div class="form-group row">
    <label for="inputTitle" class="col-sm-2 col-form-label">
        @lang('study.title_field.label')
    </label>
    <div class="col-sm-10">
        <input 
            type="text" class="form-control" id="inputTitle" name="title"
            value="{{ old('title', isset($b->title) ? $b->title : null) }}"
            placeholder="@lang('study.title_field.placeholder')" required>
    </div>
</div>  --}}