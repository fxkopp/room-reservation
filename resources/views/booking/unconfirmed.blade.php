@extends('layouts.instant')

@section('content')
<div class="container">
    <div class="py-5 text-center">
        <div class="d-block mx-auto mb-4" style="font-size:7em; color:#17a2b8">
            <i class="far fa-calendar-alt"></i>
        </div>
        <h2>Buchung angefragt!</h2>
        <p class="lead">
            Der zuständige Stockwerkssprecher wurde informiert.
            <br/>
            <b>Bitte beachte:</b> Die Buchung ist erst nach Bestätigung durch den 
            Stockwerkssprecher gültig.
        </p>

    </div>
</div>
@endsection