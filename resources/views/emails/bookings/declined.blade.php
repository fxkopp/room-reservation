@component('mail::message')
# Ablehnung deiner Buchung

Hallo {{ $booking->tenant_name }},
<br/><br/>
deine Buchungsanfrage für {{ $booking->room->name }} <br/>
am {{ $booking->start->format('l, d.m.Y') }} 
von {{ $booking->start->format('H:i') }} bis {{ $booking->end->format('H:i') }} Uhr <br/>
wurde von <b>{{ $booking->decidedBy->name }}</b> abgelehnt.
<br/><br/>
<b>Grund:</b> {{ $booking->reason }}

Diese E-Mail wurde automatisch erstellt.<br>
{{-- {{ config('app.name') }} --}}
@endcomponent
