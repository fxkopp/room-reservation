@component('mail::message')
# Buchungsanfrage für {{ $booking->room->name }}

Hallo {{ $user->name }},
<br/><br/>
es gibt eine neue Buchungsanfrage: <br/> 
<b>Von</b> {{ $booking->tenant_name }} (Zimmer {{ $booking->tenant_room_number }}) <br />
<b>Für</b> {{ $booking->start->format('l, d.m.Y') }} 
von {{ $booking->start->format('H:i') }} bis {{ $booking->end->format('H:i') }} Uhr.

@if (!$booking->tenant_message !== null)
<b>Grund:</b> {{ $booking->tenant_message }}
@endif

@component('mail::button', ['url' => route('bookings.decide', [
    'booking' => $booking,
    'user' => $user,
    'hash' => $booking->getAuthorizationHash($user),
])])
Anfrage bearbeiten
@endcomponent

Diese E-Mail wurde automatisch erstellt.<br>
{{-- {{ config('app.name') }} --}}
@endcomponent
