@component('mail::message')
# Annahme deiner Buchung

Hallo {{ $booking->tenant_name }},
<br/><br/>
deine Buchungsanfrage für {{ $booking->room->name }} <br/>
am {{ $booking->start->format('l, d.m.Y') }} 
von {{ $booking->start->format('H:i') }} bis {{ $booking->end->format('H:i') }} Uhr <br/>
wurde von <b>{{ $booking->decidedBy->name }}</b> genehmigt.
<br/><br/>
@if ($booking->handover_type == 'in_person')
Die <b>Schlüsselübergabe</b> erfolgt persönlich durch {{ $booking->decidedBy->name }}
am {{ $booking->handover_date->format('l, d.m.Y H:i') }} vor {{ $booking->room->name }}.
@else
Der <b>Schlüssel</b> wird bis {{ $booking->handover_date->format('l, d.m.Y H:i') }} Uhr
in deinen Briefkasten ({{ $booking->tenant_room_number }}) eingeworfen.
@endif
Die <b>Kaution</b> beträgt {{ str_replace('.', ',', $booking->room->deposit) }} Euro.<br />

@if ($booking->room->terms_of_use !== null)
<b>Besondere Vergabebedingungen:</b>
{!! $booking->room->terms_of_use !!}
@endif

Diese E-Mail wurde automatisch erstellt.<br>
{{-- {{ config('app.name') }} --}}
@endcomponent
