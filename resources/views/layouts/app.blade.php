<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('images/logo.png') }}" width="66" height="30" class="img-fluid"/>
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            {{-- <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li> --}}
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Content -->
        <main class="py-2">
            @yield('content')
        </main>

        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container py-5">
                <div class="row">
                    <div class="col-12 col-md">
                        <img src="{{ asset('images/logo.png') }}" width="99" height="45"/>              
                        <small class="d-block mb-3 text-muted">
                            {{ \Carbon\Carbon::now()->format('Y') }} &copy; JHH<br/>
                            made with <i class="fas fa-coffee"></i> in Regensburg/K&ouml;nigswiesen 
                        </small>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Buchungssystem</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="{{ route('welcome') }}">Übersicht / Kalender</a></li>
                            <li><a class="text-muted" href="{{ route('bookings.create') }}">Raum buchen</a></li>
                            <li><a class="text-muted" href="{{ route('termsOfUse') }}">Vergabebedingungen</a></li>                            
                            {{-- <li><a class="text-muted" href="#">Stockwerksprecher Login</a></li> --}}
                        </ul>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Über das JHH</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="http://hiltnerheim.de/content_tutoren.php" target="_blank">
                                Tutorien
                            </a></li>
                            <li><a class="text-muted" href="http://hiltnerheim.de/content_programm.php" target="_blank">
                                Semesterplan
                            </a></li>
                            <li><a class="text-muted" href="http://hiltnerheim.de/content_news.php" target="_blank">
                                Wichtige Termine
                            </a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Kontakt</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="http://www.hiltnerheim.de/content_ansprechpartner.php" target="_blank">
                                Der Heimleiter
                            </a></li>
                            <li><a class="text-muted" href="http://hiltnerheim.de/content_senioren.php" target="_blank">
                                Die Senioren
                            </a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Rechtliches</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="http://hiltnerheim.de/content_impressum.php" target="_blank">Impressum</a></li>
                            {{-- <li><a class="text-muted" href="#">Datenschutz</a></li>
                            <li><a class="text-muted" href="#">Nutzungsbedingungen</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>    
    <script defer src="{{ asset('js/app.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script defer src="{{ asset('js/moment.js') }}"></script>    
    <script defer src="{{ asset('js/fullcalendar.js') }}"></script>
    <script defer src="{{ asset('js/fullcalendar-de.js') }}"></script>
    <script defer>
        @yield('custom-scripts')
    </script>
</body>
</html>
