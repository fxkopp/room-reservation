@extends('layouts.instant')

@section('content')
<div class="container">
    <div class="py-5 text-center">
        <div class="d-block mx-auto mb-4" style="font-size:7em; color:#17a2b8">
            <i class="fas fa-envelope-open"></i>
        </div>
        <h2>Fast geschafft!</h2>
        <p class="lead">
            Da dies deine erste Raumbuchung ist, musst du deine Hanynummer best&auml;tigen.
            <br />
            In Kürze senden wir einen Verifikationscode per SMS an deine Nummer {{ $booking->tenant_phone }}.
        </p>

        <div class="mt-5 row">
            <div class="col-5 mx-auto">
                <!-- Error Messages -->
                @include('shared.errors')
                @if ($message = Session::get('twilioError'))
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('twilioSuccess'))
                    <div class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                @endif

                <!-- Form -->
                <form method="POST" action="{{ route('verify.checkCode', ['booking' => $booking]) }}">

                    <!-- CSRF token -->
                    {{ csrf_field() }}

                    <!-- Verification Code -->
                    <div class="form-group">
                        <label for="verificationCode" class="sr-only">
                            Verifikationscode
                        </label>
                        <input 
                            type="number" class="form-control form-control-lg" id="verificationCode"
                            name="verification_code"
                            value="{{ old(
                                'verification_code', 
                                isset($booking->verification_code) ? $booking->verification_code : null
                            ) }}"
                            pattern="[0-9]{4}" placeholder="Verifikationscode" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Check code</button>
                        <a class="btn btn-link btn-lg btn-block" 
                            href="{{ route('verify.resendCode', ['booking' => $booking]) }}">
                            Erneut senden?
                        </a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection